class Cromper {
    /*properties*/
    params = {
        height: 300,
        containerPadding: '12px 0',
        imageButtonsFunc: this.addImageButtons,
        centered: true,
        containersBgColor : '#ededed'
    };
    mousePos = {
        x : 0,
        y : 0
    };
    mouseInVC = {
        x: 0,
        y: 0
    };
    fr = new FileReader();
    stylesheet = document.createElement('style');
    options = {
        mode: 'move'
    };
    selected = {
        layer: undefined,
        elem: undefined
    };

    constructor( id, params = {} ){
        if(! 'FileReader' in window ){
            throw new Error('FileReader not supported');
        }
        if( !'Promise' in window ){
            throw new Error('Promise not supported');
        }

        /* stylesheet creation */
        this.stylesheet.id = 'cromperStyle';
        this.stylesheet.type = 'text/css';
        document.head.appendChild( this.stylesheet );

        this.hydrate(id, params);
        this.constructDOM();

        /* create a viewport node to copy when we need a new one */
        this.viewportNode = this.createViewportCopy();
    }

    hydrate( id, params ){
        if( typeof params !== 'object' ){
            throw new Error('Params sended is not an object');
        }

        if( typeof id === 'string'  ){
            this.containerParent = document.getElementById( id );
        }
        else{
            throw new Error('Id not provided');
        }

        for(let param in params){
            if( param in this.params ){
                this.params[param] = params[param];
            }
        }

        if( this.containerParent === null ){
            throw new Error('Container (id provided) does not exist');
        }
    }

    /**
     * Dom construction. Create dynamicaly nodes to create the cropper
     */
    constructDOM(){
        this.container = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperContainer'
            }
        });

        this.viewportContainer = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className:'cromperViewportContainer cromperMoveMode',
                style:{
                    height: this.params.height+'px',
                    position: 'relative',
                    overflow: 'hidden'
                }
            }
        });

        this.layerContainer = Cromper.getDOMNode({
            node:'div',
            attrs: {
                className: 'cromperLayerContainer'
            }
        });
        this.layerContainer.addEventListener('click', this.layerContainerClickHandler);

        this.viewportLayerContainer = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperViewportLayerContainer',
                dataset: {
                    title: 'Viewport'
                },
                style: {
                    display:'none'
                }
            }
        });

        this.elemLayerContainer = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperElemLayerContainer',
                dataset: {
                    title: 'Layers'
                },
                style: {
                    display:'none'
                }
            }
        });
        this.elemLayerContainer.addEventListener('drop', this.layerContainerDropHandler);
        this.elemLayerContainer.addEventListener('dragover', this.layerContainerDragoverHandler);

        this.optionsContainer = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperOptionsContainer',
                style: {
                    display:'none'
                }
            }
        });

        Cromper.addStyleRule( this.stylesheet, {
            'div.cromperContainer': {
                'flex-wrap': 'wrap',
                position:'relative'
            },
            'div.cromperContainer::before': {
                content: '\'\'',
                position:'absolute',
                top: '33%',
                left: 0,
                right:'212px',
                bottom: '33%',
                'border-top' : '2px dashed #EDEDED',
                'border-bottom' : '2px dashed #EDEDED',
                'z-index': 1,
                'pointer-events': 'none'
            },
            'div.cromperContainer::after': {
                content: '\'\'',
                position:'absolute',
                top: 0,
                left: '33%',
                right:'calc(33% + 212px)',
                bottom: 0,
                'border-left' : '2px dashed #EDEDED',
                'border-right' : '2px dashed #EDEDED',
                'z-index': 0,
                'pointer-events': 'none'
            },
            'div.cromperViewportContainer.canDrop::after': {
                'content' : '\'You can drop here\'',
                'position': 'absolute',
                'top' : '0',
                'left' : '0',
                'right' : '0',
                'bottom' : '0',
                'background-color' : 'black',
                'color' : 'white',
                'display' : 'flex',
                'justify-content' : 'center',
                'align-items' : 'center',
                'font-size' : '15px'
            },
            'div.cromperViewportContainer': {
                'background-color':'white',
                'box-shadow' : '0 1px 2px rgba(0, 0, 0, 0.1)'
            },
            'div.cromperContainer .cromperElemHidden': {
                display:'none !important'
            },
            'div.cromperContainer div.cromperViewportContainer .cromperElem' : {
                'pointer-events' : 'none'
            },
            'div.cromperContainer div.cromperViewportContainer.cromperMoveMode .cromperElem.cromperElemSelected': {
                'pointer-events' : 'auto',
                cursor: 'all-scroll'
            },
            'div.cromperContainer div.cromperLayerContainer div.cromperViewportLayerContainer::before, div.cromperContainer div.cromperLayerContainer div.cromperElemLayerContainer::before': {
                content: 'attr(data-title)',
                'font-family': 'Arial, sans-serif',
                'font-size': '0.8em'
            },
            'div.cromperContainer .cromperLayerContainer': {
                'overflow-y':'auto',
                'background-color': 'white',
                'box-shadow': '0 1px 2px rgba(0, 0, 0, 0.1)'
            }
        });

        this.params.imageButtonsFunc( this );

        this.addDragDropListener();
        this.container.appendChild(this.viewportContainer);
        this.layerContainer.appendChild(this.viewportLayerContainer);
        this.layerContainer.appendChild(this.elemLayerContainer);
        this.container.appendChild(this.layerContainer);
        this.container.appendChild( this.optionsContainer );
        this.containerParent.appendChild( this.container );

        this.createOptions();

        this.createLayerCopy();
        this.createLayerPlaceholder();
    }
    addImageButtons( inst ){
        let buttonTxtContainer = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperAddImgButtonContainer',
                style:{
                    display:'flex',
                    flexDirection:'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height:'100%'
                }
            }
        });

        inst.addButtonsContainer = buttonTxtContainer;

        let Txt = Cromper.getDOMNode({
            node:'p',
            attrs: {
                style:{
                    textAlign: 'center',
                    margin:'0',
                    padding:'0'
                }
            },
            props:{
                textContent: 'Drop image here or'
            }
        });

        let inputContainer = Cromper.getDOMNode({
            node:'div',
            attrs: {
                className:'comperInputContainer'
            }
        });

        let imgFileInput = Cromper.getDOMNode({
            node:'input',
            attrs:{
                type: 'file',
                id:'cromperInputAddFile',
                accept: 'image/*',
                style: {
                    width:'0.1px',
                    height: '0.1px',
                    position:'absolute',
                    zIndex: '-100',
                    top:'9999',
                    left:'9999',
                    visibility:'hidden'
                }
            },
            props: {
                multiple: true
            }
        });

        let imgFileInputLabel = Cromper.getDOMNode({
            node:'label',
            attrs:{
                style:{
                    padding: '12px',
                    fontFamily:'arial',
                    fontSize:'14px',
                    display:'block',
                    cursor:'pointer',
                    border:'1px solid grey',
                    marginTop: '12px'
                }
            },
            props: {
                htmlFor: 'cromperInputAddFile',
                textContent:'Load an image'
            }
        });

        imgFileInput.addEventListener('change', inst.showImageHandler);

        inputContainer.appendChild(imgFileInput);
        inputContainer.appendChild(imgFileInputLabel);
        buttonTxtContainer.appendChild(Txt);
        buttonTxtContainer.appendChild(inputContainer);

        inst.viewportContainer.appendChild( buttonTxtContainer );
        inst.buttonsContainer = buttonTxtContainer;
    }
    createOptions(){
        Cromper.addStyleRule(this.stylesheet, {
            'div.cromperOptionsContainer': {
                'flex-basis': '100%'
            }
        });

        this.createModeOption();
        this.createZoomOption();
        this.createGetcropOption();
    }
    createModeOption(){
        /* croper mode, slide or crop mode */
        let optionMode = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperOptionContainer switch'
            }
        });

        let optionModeInput = Cromper.getDOMNode({
            node: 'input',
            attrs: {
                type:'checkbox',
                id:'cromperModeInput'
            },
            props: {
                checked: true
            }
        });
        optionModeInput.addEventListener('change', this.optionModeChangeHandler);

        let optionModelabel = Cromper.getDOMNode({
            node: 'label',
            attrs: {
                htmlFor: 'cromperModeInput',
                classname: 'cromperOption'
            }
        });

        let optionModeSwitch = Cromper.getDOMNode({
            node: 'span',
            attrs: {
                className: 'cromperOptionSwitch'
            }
        });


        optionModelabel.appendChild(document.createTextNode('Move'));
        optionModelabel.appendChild(optionModeSwitch);
        optionModelabel.appendChild(document.createTextNode('Crop'));
        optionMode.appendChild(optionModeInput);
        optionMode.appendChild(optionModelabel);

        this.optionsContainer.appendChild( optionMode );
    }
    createZoomOption(){
        let inputRange = Cromper.getDOMNode({
            node: 'input',
            attrs:{
                type: 'range',
                min: '0.0001',
                max: '1.5',
                step: '0.0001',
                id: 'cromperZoomOption'
            }
        });

        inputRange.addEventListener('input', this.inputZoomChangeHandler);

        this.optionsContainer.appendChild( inputRange );
    }
    createGetcropOption(){
        let getCropOptionNode = Cromper.getDOMNode({
            node:'button',
            attrs:{
                className:'cromperOption',
                type:'button'
            }
        });

        getCropOptionNode.textContent = 'Get cropping';
        getCropOptionNode.addEventListener('click', this.getCropClickHandler);

        this.optionsContainer.appendChild( getCropOptionNode );
    }

    /* layers functions */
    createLayerPlaceholder(){
        let layerPlaceholder = Cromper.getDOMNode({
            node: 'p',
            attrs: {
                style: {
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height:'100%',
                    width:'100%',
                    boxSizing: 'border-box',
                    padding: '12px',
                    textAlign: 'center',
                    margin: 0
                },
                className: 'cromperLayerPlaceholder'
            }
        });

        if( document.documentElement.lang === 'fr' ){
            layerPlaceholder.textContent = 'Ajouter une image/zone de crop pour ajouter un calque';
        }
        else{
            layerPlaceholder.textContent = 'Add an image or a crop zone to add a layer';
        }

        this.layerContainer.insertBefore( layerPlaceholder, this.layerContainer.firstElementChild );
    }
    createLayerCopy(){
        Cromper.addStyleRule(this.stylesheet, {
            'div.cromperLayerDragenter': {
                position:'relative'
            },
            'div.cromperLayerDragenter *': {
                'pointer-events': 'none'
            },
            'div.cromperLayerDragenter.cromperLayerDraghovered': {
                'padding-top' : '40px'
            },
            'div.cromperLayerDragenter.layerSelected' : {
                'background-color': 'rgba(0, 0, 0, 0.2)'
            },
            'div.cromperLayerDragenter.cromperLayerDraghovered::before' : {
                content:'\'\'',
                position: 'absolute',
                left:0,
                right:0,
                top: '10px',
                border:'2px dashed black',
                height: '20px'
            },
            'div.cromperLayer' : {
                padding: '6px',
                'box-sizing': 'border-box',
                display: 'flex',
                'justify-content': 'center',
                'align-items': 'center',
                cursor: 'pointer !important'
            },
            'div.cromperLayer div.cromperLayerCheckboxContainer': {

            },
            'div.cromperLayer div.cromperLayerCheckboxContainer input.cromperLayerCheckbox': {

            },
            'div.cromperLayer div.cromperLayerCheckboxContainer label.cromperLayerlabel': {

            },
            'div.cromperLayer .cromperLayerIconContainer': {
                display: 'flex',
                'justify-content' : 'center',
                'align-items' : 'center',
                width:'60px;',
                height: '60px',
                'border-radius': '50%',
                overflow: 'hidden'
            },
            'div.cromperLayer .cromperLayerIconContainer .cromperLayerIcon': {
                flex: '0 0 auto',
                width:'100%',
                height: '100%'
            },
            'p.cromperLayerName': {
                'font-size': '0.8em'
            },
            'time.cromperLayerCreatedAt': {
                'font-size': '0.6em'
            }
        });

        let layerContainer = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperLayerDragenter'
            },
            props: {
                draggable: true
            }

        });

        let layerCopy = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperLayer'
            }
        });

        let layerCheckboxContainer = Cromper.getDOMNode({
            node:'div',
            attrs: {
                className: 'cromperLayerCheckboxContainer',
            }
        });

        let layerCheckbox = Cromper.getDOMNode({
            node: 'input',
            attrs: {
                type: 'checkbox',
                className: 'cromperLayerCheckbox'
            },
            props: {
                checked: true
            }
        });

        let layerIconContainer = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperLayerIconContainer'
            }
        });

        let layerIcon = Cromper.getDOMNode({
            node: 'img',
            attrs: {
                className: 'cromperLayerIcon'
            }
        });

        let layerInfos = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperLayerInfosContainer'
            }
        });

        let layerName = Cromper.getDOMNode({
            node : 'p',
            attrs: {
                className: 'cromperLayerName'
            }
        });

        let layerTime = Cromper.getDOMNode({
            node : 'time',
            attrs: {
                className: 'cromperLayerCreatedAt'
            }
        });

        layerCheckboxContainer.appendChild(layerCheckbox);
        layerInfos.appendChild(layerName);
        layerInfos.appendChild(layerTime);
        layerIconContainer.appendChild( layerIcon );
        layerCopy.appendChild( layerCheckboxContainer );
        layerCopy.appendChild( layerIconContainer );
        layerCopy.appendChild( layerInfos );
        layerContainer.appendChild( layerCopy );

        this.layerCopy = layerContainer;
    }
    createLayer( date, idPrefix, blobUrl ){
        let layerContainer = this.layerCopy.cloneNode( this.layerCopy );
        let layer = layerContainer.firstElementChild;

        layerContainer.id = 'cromperLayer'+date.getTime();

        layerContainer.addEventListener('dragstart', this.layerDragstartHandler);
        layerContainer.addEventListener('dragend', this.layerDragendHandler);
        layerContainer.addEventListener('dragenter', this.layerDragenterdHandler);
        layerContainer.addEventListener('dragleave', this.layerDragleaveHandler);

        let day = date.getDate() < 10 ? '0'+String(date.getDate()) : date.getDate();
        let month = date.getMonth() < 10 ? '0'+String(date.getMonth()+1) : date.getMonth();

        layer.firstElementChild.firstElementChild.addEventListener('change', this.layerCheckboxChangeHandler);
        layer.firstElementChild.firstElementChild.addEventListener('click', this.layerCheckboxClickHandler);

        layerContainer.dataset.link = idPrefix+date.getTime();
        layerContainer.dataset.linktime = date.getTime();
        layer.firstElementChild.firstElementChild.value = 'cromperElem'+date.getTime();
        layer.children[1].firstElementChild.onLoad = function(){
            URL.revokeObjectURL( blobUrl );
        };
        layer.children[1].firstElementChild.src = blobUrl;
        layer.children[2].children[0].textContent = 'Img '+date.getTime();
        layer.children[2].children[1].textContent = 'Created at '+day+'/'+month+'/'+date.getFullYear();

        if( idPrefix === 'cromperElem'){
            if( this.elemLayerContainer.style.display === 'none'  ){
                this.elemLayerContainer.removeAttribute('style');
            }

            this.elemLayerContainer.insertBefore( layerContainer, this.elemLayerContainer.firstElementChild );
        }
        else{
            if( this.viewportLayerContainer.style.display === 'none' ){
                this.viewportLayerContainer.removeAttribute('style');
            }

            this.viewportLayerContainer.insertBefore( layerContainer, this.viewportLayerContainer.firstElementChild );
        }

        this.selectLayerElem( layerContainer.dataset.link, layerContainer.dataset.linktime );
    }

    /* event handlers */
    showImageHandler = ( event ) => {
        let files = event.target.files;
        let filesL = files.length;
        for(let i = 0; i < filesL; i++){
            this.showImage( event.target.files[i] );
        }
    };
    viewportContainerDragoverHandler = (event) => {
        event.preventDefault();
    };
    viewportContainerDragenterHandler = (event) => {
        event.stopPropagation();
        this.viewportContainer.classList.toggle('canDrop');
    };
    viewportContainerDragleaveHandler = (event) => {
        event.stopPropagation();
        this.viewportContainer.classList.toggle('canDrop');
    };
    viewportContainerDropHandler = (event) => {
        event.preventDefault();
        this.viewportContainer.classList.toggle('canDrop');

        let errors = 0;
        let files = event.dataTransfer.files;
        let filesL = files.length;
        for(let i = 0; i < filesL; i++){
            if( files[i].type.indexOf('image') === -1 ){
                errors++;
            }
        }

        if( errors > 0 ){
            if( filesL > 1 ){
                alert('One added files is not an image');
            }
            else{
                alert('The added file is not an image');
            }
        }
        else{
            for(let i = 0; i < filesL; i++){
                this.showImage( files[i] );
            }
        }
    };
    bodyMousemoveHandlerResizeViewport = (e) => {
        let posInVC = {
            top: e.clientY - this.viewportContainerPos.top,
            left: e.clientX - this.viewportContainerPos.left,
            right: this.viewportContainerPos.right - e.clientX,
            bottom: this.viewportContainerPos.bottom - e.clientY
        };

        if( this.firstViewportPos.x < posInVC.left ){
            this.viewport.style.right = posInVC.right+'px';
            this.viewport.dataset.right = posInVC.right;
        }
        else{
            this.viewport.style.left = posInVC.left+'px';
            this.viewport.dataset.left = posInVC.left;
        }

        if( this.firstViewportPos.y < posInVC.top ){
            this.viewport.style.bottom = posInVC.bottom+'px';
            this.viewport.dataset.bottom = posInVC.bottom;
        }
        else{
            this.viewport.style.top = posInVC.top+'px';
            this.viewport.dataset.top = posInVC.top;
        }
    };
    bodyMouseupHandlerResizeViewportRemove = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizeViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerResizeViewportRemove);
    };
    layerCheckboxChangeHandler = (e) => {
        document.getElementById( e.target.value ).classList.toggle('cromperElemHidden');
    };
    layerCheckboxClickHandler = (e) => {
        e.stopPropagation();
    };
    layerContainerClickHandler = (e) => {
        e.stopPropagation();

        let elem = e.target;
        this.selectLayerElem( elem.dataset.link, elem.dataset.linktime );
    };
    layerContainerDragoverHandler = (e) => {
        e.preventDefault();
    };
    layerContainerDropHandler = (e) => {
        e.preventDefault();
        let id = e.dataTransfer.getData("text/plain");
        e.target.classList.remove('cromperLayerDraghovered');

        if( e.target.classList.contains('cromperLayerDragenter') && id !== e.target.id ){
            let layerContainer = document.getElementById( id );
            this.elemLayerContainer.insertBefore( layerContainer, e.target );

            let zIndex = 1;

            for(let i = this.elemLayerContainer.children.length - 1; i >= 0; i--){
                document.getElementById( this.elemLayerContainer.children[i].dataset.link ).style.zIndex = zIndex;
                zIndex++;
            }
        }
    };
    layerDragstartHandler = (e) => {
        e.target.removeEventListener('dragenter', this.layerDragenterdHandler);
        e.target.removeEventListener('dragleave', this.layerDragleaveHandler);

        e.target.style.opacity = 0.2;
        e.dataTransfer.setData('text/plain', e.target.id);
        e.dataTransfer.dropEffect = 'move';
    };
    layerDragendHandler = (e) => {
        e.target.addEventListener('dragenter', this.layerDragenterdHandler);
        e.target.addEventListener('dragleave', this.layerDragleaveHandler);

        e.target.style.opacity = 1;
    };
    layerDragenterdHandler = (e) => {
        e.stopImmediatePropagation();
        e.target.classList.add('cromperLayerDraghovered');
    };
    layerDragleaveHandler = (e) => {
        e.stopImmediatePropagation();
        e.target.classList.remove('cromperLayerDraghovered');
    };

    /* options event handlers */
    optionModeChangeHandler = () => {
        if( this.options.mode === 'move' ){
            this.options.mode = 'drawZone';
            this.viewportContainer.classList.remove('cromperMoveMode');
        }
        else{
            this.options.mode = 'move';
            this.viewportContainer.classList.add('cromperMoveMode');
        }
    };
    getCropClickHandler = (e) => {
        e.preventDefault();

        let canvasNode = Cromper.getDOMNode({
            node:'canvas',
            attrs:{
                id: 'cromperCanvas',
                style: {
                    width: this.viewport.clientWidth+'px',
                    height: this.viewport.clientHeight+'px',
                    border: '1px solid black'
                }
            },
            props: {
                width: this.viewport.clientWidth,
                height: this.viewport.clientHeight
            }
        });

        let elems = document.querySelectorAll('img.cromperElem');

        if( elems.length > 1 ){
            elems = Array.from(elems);
            elems.sort(function(a, b){
                let pos1 = parseInt(a.style.zIndex);
                let pos2 = parseInt(b.style.zIndex);

                return pos1 - pos2;
            });
        }

        let canvasCtx = canvasNode.getContext('2d');

        elems.forEach((curImg) => {
            let curImgPos = curImg.getBoundingClientRect();
            let viewportPos = this.viewport.getBoundingClientRect();

            /* getting the real values image */
            let scaleFloat = parseFloat(curImg.dataset.scale);

            let sx = ( viewportPos.left - curImgPos.left ) / scaleFloat;
            let sy = ( viewportPos.top - curImgPos.top ) / scaleFloat;

            let widthTest = this.viewport.clientWidth / scaleFloat;
            let heightTest = this.viewport.clientHeight / scaleFloat;

            canvasCtx.drawImage( curImg, sx, sy, widthTest, heightTest, 0, 0, this.viewport.clientWidth, this.viewport.clientHeight);
        });

        document.body.appendChild( canvasNode );
    };
    inputZoomChangeHandler = (e) => {
        let elem = this.selected.elem;

        let transformVal = elem.style.transform.split(') ');

        if( transformVal[1].indexOf(')') !== -1 ){
            var lastChar = ')';
        }
        else{
            var lastChar = '';
        }
        transformVal[1] = 'scale('+e.target.value+lastChar;

        elem.dataset.scale = e.target.value;
        elem.style.transform = transformVal.join(') ');
    };

    /* viewport creation. There could only be one at a time */
    createViewportCopy(){
        let viewport = Cromper.getDOMNode({
            node: 'div',
            attrs: {
                className: 'cromperViewport'
            }
        });

        let cromperManipulatorTop = Cromper.getDOMNode({
            node: 'span',
            attrs:{
                className: 'cromperManipulator top'
            }
        });

        let cromperManipulatorRight = cromperManipulatorTop.cloneNode(true);
        cromperManipulatorRight.className = 'cromperManipulator right';
        let cromperManipulatorBottom = cromperManipulatorTop.cloneNode(true);
        cromperManipulatorBottom.className = 'cromperManipulator bottom';
        let cromperManipulatorLeft = cromperManipulatorTop.cloneNode(true);
        cromperManipulatorLeft.className = 'cromperManipulator left';
        let cromperManipulatorTopLeft = cromperManipulatorTop.cloneNode(true);
        cromperManipulatorTopLeft.className = 'cromperManipulator topleft';
        let cromperManipulatorTopRight = cromperManipulatorTop.cloneNode(true);
        cromperManipulatorTopRight.className = 'cromperManipulator topright';
        let cromperManipulatorBottomRight = cromperManipulatorTop.cloneNode(true);
        cromperManipulatorBottomRight.className = 'cromperManipulator bottomright';
        let cromperManipulatorBottomLeft = cromperManipulatorTop.cloneNode(true);
        cromperManipulatorBottomLeft.className = 'cromperManipulator bottomleft';
        let cromperManipulatorCenter = cromperManipulatorTop.cloneNode(true);
        cromperManipulatorCenter.className = 'cromperManipulator center';

        viewport.appendChild(cromperManipulatorTop);
        viewport.appendChild(cromperManipulatorRight);
        viewport.appendChild(cromperManipulatorBottom);
        viewport.appendChild(cromperManipulatorLeft);
        viewport.appendChild(cromperManipulatorTopLeft);
        viewport.appendChild(cromperManipulatorTopRight);
        viewport.appendChild(cromperManipulatorBottomRight);
        viewport.appendChild(cromperManipulatorBottomLeft);
        viewport.appendChild(cromperManipulatorCenter);

        /* add the style to the specified style node */
        /* viewport manipulator style */
        let manipulatorSize = 8;
        let manipulatorColor = 'blue';
        Cromper.addStyleRule(this.stylesheet, {
            'div.cromperViewport' : {
                position: 'absolute',
                border: '2px solid blue',
                'box-sizing': 'border-box',
                margin: '0',
                cursor: 'normal !important',
                'box-shadow':'0 0 0 999999px rgba(0, 0, 0, 0.2)',
                'pointer-events': 'none'
            },
            'div.cromperMoveMode div.cromperViewport.cromperElemSelected': {
                'pointer-events': 'auto',
                cursor: 'all-scroll'
            },
            'div.cromperViewport::before, div.cromperViewport::after':{
                content:"''",
                position:'absolute',
                transform: 'translate(-50%,-50%)',
                'z-index': '1'
            },
            'div.cromperViewport::before':{
                width:'100%',
                height:'1px',
                top:'calc(50% + 1px)',
                left:'50%',
                'border-top': '1px dashed '+manipulatorColor
            },
            'div.cromperViewport::after':{
                height:'100%',
                width:'1px',
                top:'50%',
                left:'calc(50% + 1px)',
                'border-left': '1px dashed '+manipulatorColor
            },
            'span.cromperManipulator' : {
                display: 'block',
                position:'absolute',
                width:manipulatorSize+'px',
                height:manipulatorSize+'px',
                'background-color': manipulatorColor,
                'z-index' : '2',
                'pointer-events': 'inherit'
            },
            'span.cromperManipulator.center': {
                top: 'calc(50% - '+manipulatorSize/2+'px)',
                left:'calc(50% - '+manipulatorSize/2+'px)',
                'border-radius' : '50%',
                'background-color': 'transparent',
                border:'1px solid '+manipulatorColor
            },
            'span.cromperManipulator.top': {
                bottom: 'calc(100% - '+manipulatorSize/2+'px)',
                left:'calc(50% - '+manipulatorSize/2+'px)',
                cursor: 'n-resize'
            },
            'span.cromperManipulator.right': {
                left:'calc(100% - '+manipulatorSize/2+'px)',
                top:'calc(50% - '+manipulatorSize/2+'px)',
                cursor: 'e-resize'
            },
            'span.cromperManipulator.bottom': {
                top:'calc(100% - '+manipulatorSize/2+'px)',
                left:'calc(50% - '+manipulatorSize/2+'px)',
                cursor: 'n-resize'
            },
            'span.cromperManipulator.left': {
                right:'calc(100% - '+manipulatorSize/2+'px)',
                top:'calc(50% - '+manipulatorSize/2+'px)',
                cursor: 'e-resize'
            },
            'span.cromperManipulator.topleft': {
                right:'calc(100% - '+manipulatorSize/2+'px)',
                top:'-5px',
                cursor: 'se-resize'
            },
            'span.cromperManipulator.topright': {
                left:'calc(100% - '+manipulatorSize/2+'px)',
                top:'-'+manipulatorSize/2+'px',
                cursor: 'sw-resize'
            },
            'span.cromperManipulator.bottomright': {
                right:'-'+manipulatorSize/2+'px',
                bottom:'-'+manipulatorSize/2+'px',
                cursor: 'se-resize'
            },
            'span.cromperManipulator.bottomleft': {
                left: '-'+manipulatorSize/2+'px',
                bottom: '-'+manipulatorSize/2+'px',
                cursor: 'sw-resize'
            },

        });

        return viewport;
    }
    createViewport(e) {
        if (this.viewport !== undefined) {
            document.getElementById('cromperLayer'+this.viewport.dataset.time).remove();
            this.removeViewport();
        }

        let viewportDate = new Date();

        this.viewport = this.viewportNode.cloneNode(true);
        this.viewport.addEventListener('mousedown', this.viewportMousedownHandler);

        this.viewportContainerPos = this.viewportContainer.getBoundingClientRect();

        let mouseInVC = {
            x: e.clientX - this.viewportContainerPos.left,
            y: e.clientY - this.viewportContainerPos.top
        };

        this.viewport.style.zIndex = this.getLastCromperElemZIndex();
        this.viewport.style.top = mouseInVC.y + 'px';
        this.viewport.style.left = mouseInVC.x + 'px';
        this.viewport.style.right = (this.viewportContainerPos.right - e.clientX)+'px';
        this.viewport.style.bottom = (this.viewportContainerPos.bottom - e.clientY)+'px';

        this.viewport.dataset.top = mouseInVC.y;
        this.viewport.dataset.left = mouseInVC.x;

        this.viewportContainer.appendChild(this.viewport);

        document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizeViewport);
        document.body.addEventListener('mouseup', this.bodyMouseupHandlerResizeViewportRemove);

        this.firstViewportPos = mouseInVC;

        this.viewport.id = 'cromperViewport'+viewportDate.getTime();
        this.viewport.dataset.time = viewportDate.getTime();

        this.createLayer( viewportDate, 'cromperViewport', 'test' );
    }
    removeViewport(){
        this.viewport.remove();
        this.viewport = undefined;
    }

    getLastCromperElemZIndex(){
        let elems = this.viewportContainer.querySelectorAll('.cromperElem');
        if( elems.length > 0 ){
            let lastElem = elems[elems.length - 1];
            console.log( 'je suis le lastelem', lastElem, elems);
            return parseInt(lastElem.style.zIndex) + 1;
        }
        else{
            return 1;
        }
    }


    /**
     * Add the drag & drop event listeners
     */
    addDragDropListener(){
        this.viewportContainer.addEventListener('dragover', this.viewportContainerDragoverHandler);
        this.viewportContainer.addEventListener('dragenter', this.viewportContainerDragenterHandler);
        this.viewportContainer.addEventListener('dragleave', this.viewportContainerDragleaveHandler);
        this.viewportContainer.addEventListener('drop', this.viewportContainerDropHandler);
    }

    /**
     * Add the image to the cropper
     * @param file
     */
    showImage( file ){
        var imgNode = Cromper.getDOMNode({
            node: 'img',
            attrs: {
                className: 'cromperElem cromperElemSelected',
                alt: 'Image to crop',
                style: {
                    position: 'absolute',
                    transform: 'translate(0,0)',
                    transformOrigin: '50% 50%'
                }
            }
        });

        imgNode.onload = () => {
            imgNode.style.top = 'calc(50% - '+imgNode.height/2+'px)';
            imgNode.style.left = 'calc(50% - '+imgNode.width/2+'px)';

            /* calcul the img scaling to fit in container height ir width depending on the ratio */
            if( imgNode.width > imgNode.height ){
                var imgSize = imgNode.width,
                    containerSize = this.viewportContainer.clientWidth;
            }
            else{
                var imgSize = imgNode.height,
                    containerSize = this.viewportContainer.clientHeight;
            }

            if( imgSize > containerSize ){
                var scalingValue = containerSize / imgSize;
            }
            else if( imgSize < containerSize ){
                var sizeToUse = containerSize - imgSize,
                    scalingValue = 1.00 + (sizeToUse / containerSize);
            }
            else{
                var scalingValue = 1.00;
            }

            scalingValue += 0.1;

            imgNode.style.transform += ' scale('+scalingValue+')';
            imgNode.setAttribute('width', imgNode.width);
            imgNode.setAttribute('height', imgNode.height);

            /* adding data attribute to make easy the translate calculation */
            imgNode.dataset.transx = '0';
            imgNode.dataset.transy = '0';
            imgNode.dataset.scale = scalingValue;

            let curTime = new Date();
            imgNode.id = 'cromperElem'+curTime.getTime();

            /* adding event listener to the container */
            this.viewportContainer.addEventListener('mousedown', this.viewportContainerMousedownHandler);

            let lastIndex = this.getLastCromperElemZIndex();
            imgNode.style.zIndex = lastIndex;

            if( this.viewport !== undefined ){
                this.viewport.style.zIndex = lastIndex + 1;
            }

            this.viewportContainer.appendChild( imgNode );

            if( this.layerContainer.firstElementChild.style.display !== 'none' ){
                this.layerContainer.firstElementChild.style.display = 'none';
            }
            if( this.addButtonsContainer.style.display !== 'none' ){
                this.addButtonsContainer.style.display = 'none';
            }

            this.createLayer( curTime, 'cromperElem', blobUrl );

            document.getElementById('cromperZoomOption').dataset.elem = imgNode.id;

            this.optionsContainer.removeAttribute('style');
        };

        let blobUrl = URL.createObjectURL( file );
        imgNode.src = blobUrl;
    }

    viewportContainerMousedownHandler = (e) => {
        e.preventDefault();

        if( this.options.mode === 'move' ){

            /* setting the mousepos when click occur */
            this.mousePos.x = event.clientX;
            this.mousePos.y = event.clientY;

            /* moving */
            if( e.target.nodeName === 'IMG' ){
                this.currentImgNode = event.target;

                document.body.addEventListener('mousemove', this.bodyMousemoveHandlerMoveImg);
                document.body.addEventListener('mouseup', this.bodyMouseupHandler);
            }
            else{
                if( e.target.classList.contains('cromperManipulator') ){
                    /* resize the viewport */
                    let classname = e.target.className.split(' ')[1];

                    switch( classname ){
                        case 'top':
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizeTopViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeTopViewport);
                            break;
                        case 'bottom' :
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizeBottomViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeBottomViewport);
                            break;
                        case 'left':
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizeLeftViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeLeftViewport);
                            break;
                            break;
                        case 'right':
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizeRightViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeRightViewport);
                            break;
                            break;
                        case 'topleft':
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizetTopLeftViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeTopLeftViewport);
                            break;
                        case 'topright':
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizetTopRightViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeTopRightViewport);
                            break;
                        case 'bottomleft':
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizetBottomLeftViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeBottomLeftViewport);
                            break;
                        case 'bottomright':
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerResizetBottomRightViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeBottomRightViewport);
                            break;
                        default:
                            document.body.addEventListener('mousemove', this.bodyMousemoveHandlerMoveViewport);
                            document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveMoveViewport);
                    }
                }
                else{
                    /* move the viewport */
                    document.body.addEventListener('mousemove', this.bodyMousemoveHandlerMoveViewport);
                    document.body.addEventListener('mouseup', this.bodyMouseupHandlerRemoveMoveViewport);
                }
            }
        }
        else{
            this.createViewport( e );
        }
    };

    bodyMouseupHandler = () => {
        document.body.removeEventListener( 'mousemove', this.bodyMousemoveHandlerMoveImg );
        document.body.removeEventListener( 'mouseup', this.bodyMouseupHandler );
    };
    bodyMouseupHandlerRemoveMoveViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerMoveViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveMoveViewport);
    };
    bodyMouseupHandlerRemoveResizeTopViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizeTopViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeTopViewport);
    };
    bodyMouseupHandlerRemoveResizeBottomViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizeBottomViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeBottomViewport);
    };
    bodyMouseupHandlerRemoveResizeLeftViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizeLeftViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeLeftViewport);
    };
    bodyMouseupHandlerRemoveResizeRightViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizeRightViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeRightViewport);
    };
    bodyMouseupHandlerRemoveResizeTopLeftViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizetTopLeftViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeTopLeftViewport);
    };
    bodyMouseupHandlerRemoveResizeTopRightViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizetTopRightViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeTopRightViewport);
    };
    bodyMouseupHandlerRemoveResizeBottomLeftViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizetBottomLeftViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeBottomLeftViewport);
    };
    bodyMouseupHandlerRemoveResizeBottomRightViewport = () => {
        document.body.removeEventListener('mousemove', this.bodyMousemoveHandlerResizetBottomRightViewport);
        document.body.removeEventListener('mouseup', this.bodyMouseupHandlerRemoveResizeBottomRightViewport);
    };

    bodyMousemoveHandlerMoveImg = (e) => {
        let {translateX, translateY} = this.getMouseMoveVals(e.clientX, e.clientY);

        /* add values to current translate val */
        let newValX = parseFloat(this.currentImgNode.dataset.transx) + translateX;
        let newValY = parseFloat(this.currentImgNode.dataset.transy) + translateY;
        this.currentImgNode.dataset.transx = newValX;
        this.currentImgNode.dataset.transy = newValY;

        this.currentImgNode.style.transform = 'translate3d('+newValX+'px,'+newValY+'px, 0px) scale('+this.currentImgNode.dataset.scale+')';
    };
    bodyMousemoveHandlerMoveViewport = (e) => {
        let {translateX, translateY} = this.getMouseMoveVals(e.clientX, e.clientY);

        let pos = {
            top: parseFloat(this.viewport.dataset.top) + translateY,
            left: parseFloat(this.viewport.dataset.left) + translateX,
            right: parseFloat(this.viewport.dataset.right) - translateX,
            bottom:parseFloat(this.viewport.dataset.bottom) - translateY
        };

        this.viewport.dataset.top = pos.top;
        this.viewport.dataset.left = pos.left;
        this.viewport.dataset.right = pos.right;
        this.viewport.dataset.bottom = pos.bottom;

        this.viewport.style.top = pos.top+'px';
        this.viewport.style.left = pos.left+'px';
        this.viewport.style.right = pos.right+'px';
        this.viewport.style.bottom = pos.bottom+'px';
    };
    bodyMousemoveHandlerResizeTopViewport = (e) => {
        let {translateY} = this.getMouseMoveVals(e.clientX, e.clientY);
        if( this.viewport.clientHeight > 1 || translateY < 0 ){

            let top = parseFloat(this.viewport.dataset.top) + translateY;

            this.viewport.dataset.top = top;
            this.viewport.style.top = top+'px';
        }
    };
    bodyMousemoveHandlerResizeBottomViewport = (e) => {
        let {translateY} = this.getMouseMoveVals(e.clientX, e.clientY);
        if( this.viewport.clientHeight > 1 || translateY > 0 ){

            let bottom = parseFloat(this.viewport.dataset.bottom) - translateY;

            this.viewport.dataset.bottom = bottom;
            this.viewport.style.bottom = bottom+'px';
        }
    };
    bodyMousemoveHandlerResizeLeftViewport = (e) => {
        let {translateX} = this.getMouseMoveVals(e.clientX, e.clientY);

        if( this.viewport.clientWidth > 1 || translateX < 0 ){

            let left = parseFloat(this.viewport.dataset.left) + translateX;

            this.viewport.dataset.left = left;
            this.viewport.style.left = left+'px';
        }
    };
    bodyMousemoveHandlerResizeRightViewport = (e) => {
        let {translateX} = this.getMouseMoveVals(e.clientX, e.clientY);

        if( this.viewport.clientWidth > 1 || translateX > 0 ){

            let right = parseFloat(this.viewport.dataset.right) - translateX;

            this.viewport.dataset.right = right;
            this.viewport.style.right = right+'px';
        }
    };
    bodyMousemoveHandlerResizetTopLeftViewport = (e) => {
        let {translateX, translateY} = this.getMouseMoveVals(e.clientX, e.clientY);


        if( this.viewport.clientHeight > 1 || translateY < 0 ){

            let top = parseFloat(this.viewport.dataset.top) + translateY;

            this.viewport.dataset.top = top;
            this.viewport.style.top = top+'px';
        }
        if( this.viewport.clientWidth > 1 || translateX < 0 ){

            let left = parseFloat(this.viewport.dataset.left) + translateX;

            this.viewport.dataset.left = left;
            this.viewport.style.left = left+'px';
        }
    };
    bodyMousemoveHandlerResizetTopRightViewport = (e) => {
        let {translateX, translateY} = this.getMouseMoveVals(e.clientX, e.clientY);


        if( this.viewport.clientHeight > 1 || translateY < 0 ){

            let top = parseFloat(this.viewport.dataset.top) + translateY;

            this.viewport.dataset.top = top;
            this.viewport.style.top = top+'px';
        }
        if( this.viewport.clientWidth > 1 || translateX > 0 ){

            let right = parseFloat(this.viewport.dataset.right) - translateX;

            this.viewport.dataset.right = right;
            this.viewport.style.right = right+'px';
        }
    };
    bodyMousemoveHandlerResizetBottomLeftViewport = (e) => {
        let {translateX, translateY} = this.getMouseMoveVals(e.clientX, e.clientY);


        if( this.viewport.clientHeight > 1 || translateY > 0 ){

            let bottom = parseFloat(this.viewport.dataset.bottom) - translateY;

            this.viewport.dataset.bottom = bottom;
            this.viewport.style.bottom = bottom+'px';
        }
        if( this.viewport.clientWidth > 1 || translateX < 0 ){

            let left = parseFloat(this.viewport.dataset.left) + translateX;

            this.viewport.dataset.left = left;
            this.viewport.style.left = left+'px';
        }
    };
    bodyMousemoveHandlerResizetBottomRightViewport = (e) => {
        let {translateX, translateY} = this.getMouseMoveVals(e.clientX, e.clientY);


        if( this.viewport.clientHeight > 1 || translateY > 0 ){

            let bottom = parseFloat(this.viewport.dataset.bottom) - translateY;

            this.viewport.dataset.bottom = bottom;
            this.viewport.style.bottom = bottom+'px';
        }
        if( this.viewport.clientWidth > 1 || translateX > 0 ){

            let right = parseFloat(this.viewport.dataset.right) - translateX;

            this.viewport.dataset.right = right;
            this.viewport.style.right = right+'px';
        }
    };

    getMouseMoveVals( posX, posY ){
        /* get the translate values */
        let translateX = posX - this.mousePos.x;
        let translateY = posY - this.mousePos.y;

        /* store the new pos */
        this.mousePos.x = posX;
        this.mousePos.y = posY;

        return { translateX, translateY };
    }

    selectLayerElem( id, time ){
        if( this.selected.elem !== undefined ){
            this.selected.elem.classList.remove('cromperElemSelected');
            this.selected.layer.classList.remove('layerSelected');
        }

        this.selected = {
            elem: document.getElementById(id),
            layer: document.querySelector('[data-linktime="'+time+'"]')
        };

        let zoomOption = document.getElementById('cromperZoomOption');
        if( this.selected.elem.classList.contains('cromperElem') ){
            if( zoomOption.disabled ){
                zoomOption.disabled = false;
            }
            zoomOption.dataset.elem = this.selected.elem.id;
            zoomOption.value = this.selected.elem.dataset.scale
        }
        else{
            zoomOption.disabled = true;
        }

        this.selected.elem.classList.add('cromperElemSelected');
        this.selected.layer.classList.add('layerSelected');
    }


    /**
     * Create a new Node
     * @param nodeInfos
     * @returns {HTMLDivElement | HTMLParagraphElement | HTMLInputElement | HTMLImageElement | HTMLLabelElement}
     */
    static getDOMNode( nodeInfos ){
        let node = document.createElement( nodeInfos.node );

        if( 'props' in nodeInfos ){
            for(let prop in nodeInfos.props){
                node[prop] = nodeInfos.props[prop];
            }

            delete nodeInfos.props;
        }

        if( 'attrs' in nodeInfos ){
            if( 'style' in nodeInfos.attrs ) {
                for( var styleProp in nodeInfos.attrs.style ){
                    node.style[styleProp] = nodeInfos.attrs.style[styleProp];
                }

                delete nodeInfos.attrs.style;
            }

            if( 'dataset' in nodeInfos.attrs ){
                for( var data in nodeInfos.attrs.dataset ){
                    node.dataset[data] = nodeInfos.attrs.dataset[data];
                }

                delete nodeInfos.attrs.dataset;
            }

            for(let attr in nodeInfos.attrs){
                if( attr in node ){
                    node[attr] = nodeInfos.attrs[attr];
                }
            }
        }

        return node;
    }

    /**
     * Add a new style rule in a style node
     * @param styleNode
     * @param selectorPropValObj
     */
    static addStyleRule( styleNode, selectorPropValObj ){
        let rule = '';

        for( let selector in selectorPropValObj ){
            let properties = selectorPropValObj[selector];
            rule += selector+'{';

            for( let property in properties ){
                rule += property+': '+properties[property]+';';
            }

            rule += '}';
        }

        styleNode.appendChild( document.createTextNode(rule) );
    }
}